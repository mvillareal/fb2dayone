require 'rubygems'
require 'libxml'
require 'date'

include LibXML

OWNER_NAME = "Maricris Villareal"

class WallPostCallbacks
	include XML::SaxParser::Callbacks

	attr_accessor :posts

	def initialize()
		@posts = Hash.new
		@isInPost = false
		@isInDate = false
		@isInContent = false
		@isMyPost = false
		@curDate = ''
	end

	def on_start_element(element, attributes)
		if element == 'p'
			@isInPost = true
		elsif element == 'div'
			if @isInPost
				if attributes['class'] == 'meta'
					@isInDate = true
				elsif attributes['class'] == 'comment'
					@isInContent = true
				end
			end	
		end
	end

	def on_characters(chars)
		if @isInDate 
			@curDate = DateTime.parse(chars).to_date().strftime("%m/%d/%Y")
		elsif @isInContent and @isMyPost
			if @posts[@curDate] then
				@posts[@curDate] = @posts[@curDate] + chars
			else
				@posts[@curDate] = chars
			end
		elsif @isInPost and chars.index(OWNER_NAME) and not chars.index(' via ')
			@isMyPost = true
		end		 	
	end

	def on_end_element(element)
		if element == 'p'
			if @posts[@curDate]
				@posts[@curDate] = @posts[@curDate] + "\n"
			end
			@isInPost = false
			@curDate = ''
		elsif element == 'div'
			@isInDate = false
			@isInContent = false
			@isMyPost = false
		end
	end
end

class PhotoCallbacks
	include XML::SaxParser::Callbacks

	attr_accessor :photos

	def initialize()
		@photos = Hash.new
		@isInPost = false
		@isInDate = false
		@curPhoto = ''
	end

	def on_start_element(element, attributes)
		if not @isInPost 
			if element == 'div' and attributes['class'] == 'block'
				@isInPost = true
			end
		else	
			if element == 'img'
				@curPhoto = attributes['src']
			elsif element == 'div' and attributes['class'] == 'meta'
				@isInDate = true
			end	
		end
	end

	def on_characters(chars)
		if @isInDate
			date = DateTime.parse(chars).to_date().strftime("%m/%d/%Y")
			@photos[date] = @curPhoto
		end		 	
	end

	def on_end_element(element)
		if element == 'div'
			@curPhoto = ''
			@isInPost = false
			@isInDate = false
		end
	end
end

class MobileUploadsCallbacks
	include XML::SaxParser::Callbacks

	attr_accessor :photos

	def initialize()
		@photos = Hash.new
		@isInPost = false
		@isInMeta = false
		@isInMetaItem = false
		@isInDateItem = false
		@isInDate = false
		@curPhoto = ''
	end

	def on_start_element(element, attributes)
		if not @isInPost 
			if element == 'div' and attributes['class'] == 'block'
				@isInPost = true
			end
		else	
			if element == 'img'
				@curPhoto = attributes['src']
			elsif element == 'table' and attributes['class'] == 'meta'
				@isInMeta = true
			elsif @isInMeta and element == 'th' 
				@isInMetaItem = true
			elsif @isInDateItem and element == 'td'
				@isInDate = true
			end	
		end
	end

	def on_characters(chars)
		if @isInMetaItem and chars == 'Taken'
			@isInDateItem = true
		elsif @isInDate
			date = Time.at(chars.to_i()).strftime("%m/%d/%Y")
			@photos[date] = @curPhoto
		end		 	
	end

	def on_end_element(element)
		if element == "td"
			@isInDate = false
		elsif element == "table"
			@isInPost = false
			@isInMeta = false
			@curPhoto = ''
		elsif element == "tr"
			@isInMetaItem = false
			@isInDateItem = false
		end
	end
end

#extract wall posts created by OWNER_NAME
wallParser = XML::SaxParser.file("data/html/wall.htm")
wallParser.callbacks = WallPostCallbacks.new()
wallParser.parse
puts "Found " + wallParser.callbacks.posts.length.to_s() + " posts."

photoParser = XML::SaxParser.file("data/html/photos.htm")
photoParser.callbacks = PhotoCallbacks.new()
photoParser.parse
photos1 = photoParser.callbacks.photos
puts "Found " + photos1.length.to_s() + " photos."

mobileUploadParser = XML::SaxParser.file("data/photos/10150152201306314/index.htm")
mobileUploadParser.callbacks = MobileUploadsCallbacks.new()
mobileUploadParser.parse
photos2 = mobileUploadParser.callbacks.photos
puts "Found " + (photos2.length).to_s() + " photos from Mobile Uploads."

timelineParser = XML::SaxParser.file("data/photos/43624541313/index.htm")
timelineParser.callbacks = MobileUploadsCallbacks.new()
timelineParser.parse
photos3 = timelineParser.callbacks.photos
puts "Found " + (photos3.length).to_s() + " photos from Timeline photos."

wallParser.callbacks.posts.each do |date, post|
	photoFile = photos1[date]

	if photoFile
		cmd = 'echo "Export from FB: ' + post + '" | dayone -d=' + date + ' -p=data/html/' + photoFile + ' new '
		photos1.delete(date)
	else
		photoFile = photos2[date]
		if photoFile
			cmd = 'echo "Export from FB: ' + post + '" | dayone -d=' + date + ' -p=data/photos/10150152201306314/' + photoFile + ' new '
			photos2.delete(date)
		else
		photoFile = photos3[date]
			if photoFile
				cmd = 'echo "Export from FB: ' + post + '" | dayone -d=' + date + ' -p=data/photos/10150152201306314/' + photoFile + ' new '
				photos3.delete(date)
			else
				cmd = 'echo "Export from FB: ' + post + '" | dayone -d=' + date + ' new '
			end
		end
	end
	puts cmd
	system(cmd)
end

photos1.each do |date, photoFile|
	cmd = 'echo "Photo Export from FB: " | dayone -d=' + date + ' -p=data/html/' + photoFile + ' new '
	puts cmd
	system(cmd)
end

photos2.each do |date, photoFile|
	cmd = 'echo "Photo Export from FB: " | dayone -d=' + date + ' -p=data/photos/10150152201306314/' + photoFile + ' new '
	puts cmd
	system(cmd)
end

photos3.each do |date, photoFile|
	cmd = 'echo "Photo Export from FB: " | dayone -d=' + date + ' -p=data/photos/10150152201306314/' + photoFile + ' new '
	puts cmd
	system(cmd)
end
